
let sorting = (array) => {
    return array.sort();
}

let compare = (a, b) => {
    return parseInt(a["PM2.5"]) > parseInt(b["PM2.5"]) ? 1 : -1;
}

let average = (nums) => {
    let sum = nums.reduce((previous, current) => current += previous);
    let avg = sum / nums.length;
    return Math.round(avg * 100) / 100;
}


module.exports = {
    sorting,
    compare,
    average
}